import 'package:flutter/material.dart';
import 'package:maxway/screens/nav_bar/nav_bar_screen.dart';

void main() => runApp(const MaxwayApp());

class MaxwayApp extends StatelessWidget {
  const MaxwayApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: NavBarScreen(),
    );
  }
}
