class ProductModel {
  String image;
  String title;
  int price;
  int countInCart;

  ProductModel(
      {required this.image,
      required this.title,
      required this.price,
      required this.countInCart});
}
