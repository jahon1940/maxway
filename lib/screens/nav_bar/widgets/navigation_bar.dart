import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class NavigationBarWidget extends StatelessWidget {
  const NavigationBarWidget(
      {super.key, required this.onChange, required this.currentIndex});

  final void Function(int) onChange;
  final int currentIndex;

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
        currentIndex: currentIndex,
        onTap: onChange,
        items: [
          BottomNavigationBarItem(
              icon: SvgPicture.asset(
                'assets/icons/bottomnav/Home.svg',
              ),
              label: 'Главная'),
          BottomNavigationBarItem(
              icon: SvgPicture.asset(
                'assets/icons/bottomnav/Buy.svg',
              ),
              label: 'Мои заказы'),
          BottomNavigationBarItem(
              icon: SvgPicture.asset(
                'assets/icons/bottomnav/Profile.svg',
              ),
              label: 'Личное')
        ]);
  }
}
