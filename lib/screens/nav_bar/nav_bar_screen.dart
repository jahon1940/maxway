import 'package:flutter/material.dart';
import 'package:maxway/screens/cart/cart_screen.dart';
import 'package:maxway/screens/home/home_screen.dart';
import 'package:maxway/screens/home/widgets/custom_search_field.dart';
import 'package:maxway/screens/nav_bar/widgets/navigation_bar.dart';
import 'package:maxway/screens/profile/profile_screen.dart';

class NavBarScreen extends StatefulWidget {
  const NavBarScreen({super.key});

  @override
  State<NavBarScreen> createState() => _NavBarScreenState();
}

class _NavBarScreenState extends State<NavBarScreen> {
  int activeIndex = 0;
  List<Widget> screens = const [HomeScreen(), CartScreen(), ProfileScreen()];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: IndexedStack(index: activeIndex, children: screens),
      bottomNavigationBar: NavigationBarWidget(
        onChange: onViewChange,
        currentIndex: activeIndex,
      ),
    );
  }

  onViewChange(int index) {
    setState(() {
      activeIndex = index;
    });
  }
}
