import 'package:flutter/material.dart';
import 'package:maxway/models/product_model.dart';
import 'package:scrollable_list_tab_scroller/scrollable_list_tab_scroller.dart';

class HomeBodyWidget extends StatelessWidget {
  HomeBodyWidget({super.key, required this.data});

  final Map<String, List<ProductModel>> data;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return ScrollableListTabScroller(
      tabBuilder: (BuildContext context, int index, bool active) => Padding(
        padding: EdgeInsets.symmetric(horizontal: 10),
        child: Text(
          data.keys.elementAt(index),
          style: !active
              ? null
              : TextStyle(fontWeight: FontWeight.bold, color: Colors.green),
        ),
      ),
      itemCount: data.length,
      itemBuilder: (BuildContext context, int index) => Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (index == 0) ...[
            Text(
              "Рекомендуем",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 20,
              ),
            ),
            SizedBox(
                height: 100,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(12),
                        child: SizedBox(
                          width: 112,
                          height: 88,
                          child: Image.asset(
                            'assets/images/image1.png',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(12),
                        child: SizedBox(
                          width: 112,
                          height: 88,
                          child: Image.asset(
                            'assets/images/image2.png',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: ClipRRect(
                        borderRadius: BorderRadius.circular(12),
                        child: SizedBox(
                          width: 112,
                          height: 88,
                          child: Image.asset(
                            'assets/images/image3.png',
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                  ],
                ))
          ],
          Text(
            data.keys.elementAt(index),
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
          ),
          GridView.builder(
            primary: false,
            shrinkWrap: true,
            padding: const EdgeInsets.all(20),
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2),
            itemCount: data.values.toList()[index].length,
            itemBuilder: (context, productIndex) {
              final products = data.values.toList()[index];
              final product = products[productIndex];
              return Container(
                child: Column(
                  children: [
                    Image.asset(product.image),
                    Text(product.title),
                    Text(product.price.toString())
                  ],
                ),
              );
            },
          )
        ],
      ),
    );
  }
}
