import 'package:flutter/material.dart';
import 'package:maxway/models/product_model.dart';
import 'package:maxway/screens/home/widgets/home_body_widget.dart';
import 'widgets/custom_search_field.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<ProductModel> burgers = [
    ProductModel(
        image: "assets/images/burger.png",
        title: "Макс Бургер",
        price: 25000,
        countInCart: 0),
    ProductModel(
        image: "assets/images/burger.png",
        title: "Макс Бургер",
        price: 25000,
        countInCart: 0),
    ProductModel(
        image: "assets/images/burger.png",
        title: "Макс Бургер",
        price: 25000,
        countInCart: 0),
    ProductModel(
        image: "assets/images/burger.png",
        title: "Макс Бургер",
        price: 25000,
        countInCart: 0),
  ];

  List<ProductModel> sandwichs = [];

  List<ProductModel> lavash = [];

  List<ProductModel> potatoes = [];

  Map<String, List<ProductModel>> data = {};

  @override
  void initState() {
    data = {
      "Бургеры": burgers,
      "Сендвич": burgers,
      "Лаваш": burgers,
      "Картошка фри": burgers
    };
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        title: CustomSearchTextField(
          fieldValue: (g) {},
        ),
      ),
      body: HomeBodyWidget(
        data: data,
      ),
    );
  }
}
